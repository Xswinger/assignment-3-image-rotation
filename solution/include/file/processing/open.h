#ifndef IMAGE_TRANSFORMER_OPEN_H
#define IMAGE_TRANSFORMER_OPEN_H

#include "status.h"
#include "stdio.h"

#include "util/util.h"

enum status open_file(FILE** file, const char* image_file_name, const char *filename, const char* read_mode);

#endif

