#ifndef IMAGE_TRANSFORMER_CLOSE_H
#define IMAGE_TRANSFORMER_CLOSE_H

#include "status.h"
#include <stdio.h>


enum status close_file(FILE* file, const char *filename);

#endif
