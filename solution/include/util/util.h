#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H

#include "bmp/structure/structure.h"

#include <stdio.h>
#include <stdlib.h>

#define ERROR_STOP return 1
#define NORMAL_STOP return 0

void print_status_message(const char *message, const char *filename);

void print_error(char *error_message);

#endif
