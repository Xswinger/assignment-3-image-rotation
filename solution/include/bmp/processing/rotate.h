#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "bmp/structure/structure.h"

#define SOURCE_DATA source.data
#define SOURCE_WIDTH source.size.width
#define SOURCE_HEIGHT source.size.height

#define TRANSFORMED_DATA transformed.data
#define TRANSFORMED_WIDTH transformed.size.width
#define TRANSFORMED_HEIGHT transformed.size.height

struct image rotate_image(const struct image source);

#endif
