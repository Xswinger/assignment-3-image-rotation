#ifndef IMAGE_TRANSFORMER_READER_H
#define IMAGE_TRANSFORMER_READER_H

#include "bmp/structure/structure.h"
#include "stdio.h"

#define SOURCE_DATA_POINTER source->data
#define SOURCE_WIDTH_POINTER source->size.width
#define SOURCE_HEIGHT_POINTER source->size.height

#define READ_OK_RETURN return READ_OK
#define ERROR_HEADER_RETURN return READ_INVALID_HEADER
#define ERROR_BITS_RETURN return READ_INVALID_BITS

enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum read_status from_bmp(FILE* source_file, struct image* source );

#endif
