#ifndef IMAGE_TRANSFORMER_WRITER_H
#define IMAGE_TRANSFORMER_WRITER_H

#include "bmp/structure/structure.h"
#include "stdio.h"

#define WRITE_OK_RETURN return WRITE_OK
#define ERROR_RETURN return WRITE_ERROR

#define TRANSFORMED_DATA_POINTER transformed->data
#define TRANSFORMED_WIDTH_POINTER transformed->size.width
#define TRANSFORMED_HEIGHT_POINTER transformed->size.height

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* transformed_file, struct image const* transformed );

#endif
