#ifndef IMAGE_TRANSFORMER_DIMENSION_H
#define IMAGE_TRANSFORMER_DIMENSION_H

#include <stddef.h>
#include <stdint.h>

struct dimensions {
    uint64_t width;
    uint64_t height;
};

#endif
