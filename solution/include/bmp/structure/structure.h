#ifndef IMAGE_TRANSFORMER_STRUCT_H
#define IMAGE_TRANSFORMER_STRUCT_H

#include  <stdint.h>

#include "dimension.h"
#include "inttypes.h"

#define IMAGE_STRUCT_SIZE sizeof(struct image)
#define PIXEL_STRUCT_SIZE sizeof(struct pixel)
#define BMP_HEADER_STRUCT_SIZE sizeof(struct bmp_header)

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

struct image {
    struct dimensions size;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

#endif

