#ifndef IMAGE_TRANSFORMER_GET_IMAGE_H
#define IMAGE_TRANSFORMER_GET_IMAGE_H


#include "bmp/processing/reader.h"
#include "file/processing/close.h"
#include "file/processing/open.h"
#include "util/util.h"

#define READ "rb"

int get_image(FILE *source_file, const char *source_image, struct image *source);

#endif //IMAGE_TRANSFORMER_GET_IMAGE_H

