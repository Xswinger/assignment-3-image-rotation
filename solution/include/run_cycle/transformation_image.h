#ifndef IMAGE_TRANSFORMER_TRANSFORMATION_IMAGE_H
#define IMAGE_TRANSFORMER_TRANSFORMATION_IMAGE_H

#include "bmp/processing/rotate.h"
#include "bmp/processing/writer.h"
#include "file/processing/close.h"
#include "file/processing/open.h"
#include "util/util.h"

#define WRITE "wb"

int transformation_image(FILE *transformed_file, const char *transformed_image, struct image transformed, struct image *source);

#endif //IMAGE_TRANSFORMER_TRANSFORMATION_IMAGE_H
