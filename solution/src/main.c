#include "stdio.h"
#include "stdlib.h"

#include "bmp/processing/rotate.h"
#include "run_cycle/get_image.h"
#include "run_cycle/transformation_image.h"
#include "util/util.h"

FILE *source_file, *transformed_file;
char *source_image, *transformed_image;
struct image *source, transformed;


int main(int argc, char **argv) {
    if ((!argv[1] || !argv[2]) && argc != 3) {
        print_error("Wrong input format (Should be: ./image-transformer <source-image> <transformed-image>)");
        ERROR_STOP;
    }

    source_image = argv[1];
    transformed_image = argv[2];
    source = malloc(IMAGE_STRUCT_SIZE);

    if (get_image(source_file, source_image, source)) {
        ERROR_STOP;
    }

    if (transformation_image(transformed_file, transformed_image, transformed, source)) {
        ERROR_STOP;
    }

    NORMAL_STOP;
}
