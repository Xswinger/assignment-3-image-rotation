#include "run_cycle/transformation_image.h"

int transformation_image(FILE *transformed_file, const char *transformed_image, struct image transformed, struct image *source) {
    if (open_file(&transformed_file, transformed_image, "transformed", WRITE)) {
        print_error("Can't open transformed file");
        ERROR_STOP;
    }

    transformed = rotate_image(*source);

    free(source->data);

    to_bmp(transformed_file, &transformed);

    free(transformed.data);

    close_file(transformed_file, "transformed");

    NORMAL_STOP;
}
