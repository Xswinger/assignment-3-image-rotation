#include "run_cycle/get_image.h"

int get_image(FILE *source_file, const char *source_image, struct image *source) {
    if (open_file(&source_file, source_image, "source", READ)) {
        print_error("Can't open source file");
        ERROR_STOP;
    }

    if (from_bmp(source_file, source)) {
        print_error("Error when read source file");
        ERROR_STOP;
    }

    close_file(source_file, "source");

    NORMAL_STOP;
}
