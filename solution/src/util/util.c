#include "util/util.h"

void print_status_message(const char *message, const char *filename) {
    printf("%s", filename);
    printf("%s\n", message);
}

void print_error(char *error_message) {
    fprintf(stderr, "%s\n", error_message);
}
