#include "file/processing/close.h"
#include "util/util.h"

enum status close_file(FILE* file, const char *filename) {
    print_status_message(": closing file...", filename);

    if (!file) {
        print_error("Error when closing file");
        return ERROR;
    }

    if (fclose(file)) {
        print_error("Error when closing file");
        return ERROR;
    }

    return SUCCESS;
}
