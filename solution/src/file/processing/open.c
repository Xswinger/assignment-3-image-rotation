#include "file/processing/open.h"

enum status open_file(FILE** file, const char* image_file_name, const char *filename, const char* read_mode) {
    print_status_message(": opening file...", filename);

    *file = fopen(image_file_name, read_mode);
    if (!*file) {
        return ERROR;
    }
    return SUCCESS;
}

