#include "stdio.h"

#include "bmp/processing/writer.h"
#include "bmp/structure/structure.h"
#include "util/util.h"

//struct bmp_header input_header;

struct bmp_header get_header(struct image const *transformed, int filling) {
    struct bmp_header header = {
            .bfType = 0x4d42,
            .bfileSize = BMP_HEADER_STRUCT_SIZE + (TRANSFORMED_HEIGHT_POINTER * (TRANSFORMED_WIDTH_POINTER + filling) * PIXEL_STRUCT_SIZE),
            .bfReserved = 0,
            .bOffBits = BMP_HEADER_STRUCT_SIZE,
            .biSize = 40,
            .biHeight = TRANSFORMED_HEIGHT_POINTER,
            .biWidth = TRANSFORMED_WIDTH_POINTER,
            .biPlanes = 0,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = TRANSFORMED_HEIGHT_POINTER * (TRANSFORMED_WIDTH_POINTER + filling) * PIXEL_STRUCT_SIZE,
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}

//Реализовать передачу заголовка исходного файла заголовку выходного не получилось, так как программа ломается на функции fseek (при условии, что закрытие файла происходит после исполнения этой функции). К тому же, это исправление немного некорректно, потому что тогад нужно передавать FILE source_file всем функциям вплоть до этой, использование в заголовочных файлах вызывает multiply references, а в .c - ошибку использования с в с. Делать ее глобальной переменной было бы неуместно

enum write_status to_bmp(FILE *transformed_file, struct image const *transformed) {

    //    fseek(source_file, 0, SEEK_SET);
//
//    if (fread(&input_header, BMP_HEADER_STRUCT_SIZE, 1, source_file) != 1) {
//        print_error("Invalid header");
//        ERROR_RETURN;
//    }

    int filling = (4 - (int) ((TRANSFORMED_WIDTH_POINTER * PIXEL_STRUCT_SIZE) % 4)) % 4;

    struct bmp_header header = get_header(transformed, filling);

    if (!fwrite(&header, BMP_HEADER_STRUCT_SIZE, 1, transformed_file)) {
        print_error("Invalid header");
        ERROR_RETURN;
    }

    for (size_t i = 0; i < TRANSFORMED_HEIGHT_POINTER; i++) {
        if (fwrite(TRANSFORMED_DATA_POINTER + i * TRANSFORMED_WIDTH_POINTER, PIXEL_STRUCT_SIZE, TRANSFORMED_WIDTH_POINTER, transformed_file) != TRANSFORMED_WIDTH_POINTER) {
            print_error("Error when write pixel");
            ERROR_RETURN;
        }

        if (fseek(transformed_file, filling, SEEK_CUR)) {
            print_error("Error when write byte");
            ERROR_RETURN;
        }
    }
    WRITE_OK_RETURN;
}
