#include "bmp/processing//rotate.h"
#include "stdlib.h"

struct image rotate_image(const struct image source) {
    struct image transformed;

    TRANSFORMED_HEIGHT = SOURCE_WIDTH;
    TRANSFORMED_WIDTH = SOURCE_HEIGHT;
    TRANSFORMED_DATA = malloc(PIXEL_STRUCT_SIZE * SOURCE_WIDTH * SOURCE_HEIGHT);

    for (size_t i = 0; i < SOURCE_HEIGHT; i++) {
        for (size_t j = 0; j < SOURCE_WIDTH; j++) {
            TRANSFORMED_DATA[j * SOURCE_HEIGHT + SOURCE_HEIGHT - i - 1] = SOURCE_DATA[i * SOURCE_WIDTH + j];
        }
    }

    return transformed;
}


