#include "bmp/processing/reader.h"
#include "bmp/structure/structure.h"
#include "stdio.h"
#include "stdlib.h"
#include "util/util.h"

struct bmp_header header;

enum read_status from_bmp(FILE *source_file, struct image *source) {

    if (fread(&header, BMP_HEADER_STRUCT_SIZE, 1, source_file) != 1) {
        print_error("Invalid header");
        ERROR_HEADER_RETURN;
    }

    SOURCE_WIDTH_POINTER = header.biWidth;
    SOURCE_HEIGHT_POINTER = header.biHeight;
    SOURCE_DATA_POINTER = malloc(PIXEL_STRUCT_SIZE * SOURCE_WIDTH_POINTER * SOURCE_HEIGHT_POINTER);

    int filling = (4 - (int) ((SOURCE_WIDTH_POINTER * PIXEL_STRUCT_SIZE) % 4)) % 4;

    fseek(source_file, header.bOffBits, SEEK_SET);

    for (size_t i = 0; i < SOURCE_HEIGHT_POINTER; i++) {

        if (fread(i * SOURCE_WIDTH_POINTER + SOURCE_DATA_POINTER, PIXEL_STRUCT_SIZE, SOURCE_WIDTH_POINTER, source_file) != SOURCE_WIDTH_POINTER) {
            print_error("Invalid when read bits");
            ERROR_BITS_RETURN;
        }

        if (fseek(source_file, filling, SEEK_CUR)) {
            print_error("Invalid when set pointer");
            ERROR_BITS_RETURN;
        }
    }

    READ_OK_RETURN;
}
